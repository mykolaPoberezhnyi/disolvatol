$(document).ready(function() {
    $( "#testimonials__carousel" ).owlCarousel({
        items: 1,
        nav: true,
        navText: ['prev', 'next'],
        dots: true,
        loop: true
    });
    $(".menu__open").click(function() {
        $('body').addClass('modal-show');
        $("#navigation").addClass("toggle");
    });
    $(".menu__close").click(function() {
        $('body').removeClass('modal-show');
        $("#navigation").removeClass("toggle");
    });
    $("body").on("click","a", function (event) {
		// event.preventDefault();
		let id  = $(this).attr('href');
        let	top = $(id).offset().top-30;
        if ( $("#navigation").hasClass('toggle') ) {
            $("#navigation").removeClass("toggle"); 
            $('body').removeClass('modal-show');
        }
        $('body,html').animate({scrollTop: top}, 1500);
    });
    $(".showcase__watchthevideo").click(function() {
        $('body').addClass('modal-show');
        $("#modal").addClass('show');
    });
    $(".modal__close").click(function() {
        closeModal();
    });
    $(".modal__overlay").click(function() {
        closeModal();
    });
    $(window).scroll(function() { 
        if ( $(window).width() > 767 ) {
            $(document).scrollTop() > 30 ? $('.top').addClass('fixedtop') : $('.top').removeClass('fixedtop');
        }
        else {
            $(document).scrollTop() > 50 ? $('.top').addClass('fixedtop') : $('.top').removeClass('fixedtop');
        }
    });
});
$('.wrapper').on('scroll', function() {
    console.log($('body').scrollTop());
})
function closeModal() {
    $('body').removeClass('modal-show');
    $("#modal").removeClass('show');
}